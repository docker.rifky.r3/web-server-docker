# web-server-docker

Sample of Web Server on docker with NGINX, multiple PHP and MYSQL/POSTGRES

Information of what is inside the compose file
- PHP (php-5.4-fpm to php-8.2-fpm) using image [chocin/php](https://hub.docker.com/r/chocin/php) bassed on [php Docker Official Image](https://hub.docker.com/_/php/)
- MYSQL (mysql-5.6 & mysql-8.0) using image [mysql](https://hub.docker.com/_/mysql)
- POSTGRES (postgres-14) using image [postgres](https://hub.docker.com/_/postgres)
- PHP xdebug port is `9005`
- NGINX is using port `80<php-version>` ex:`8082`
- MYSQL is using port `33<mysql-version>` ex:`3356`
- POSTGRES is using port `54321`

Tutorial of installation or run
- Copy `.env.sample` to `.env`
- Configure `.env` with your needed (optional)
- Execute `.\compose-up.ps1` on Windows or `docker-compose up -d <your-service>` ex: `docker-compose up -d nginx php-8.2-fpm mysql-8.0` or you can copy `compose-up.ps1` and edit it