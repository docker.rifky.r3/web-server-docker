#!/bin/bash
docker run -d --add-host host.docker.internal:host-gateway \
    --name php56-apache \
    -p 8056:80 \
    -v ../data:/var/www/html \
    -v ../config/conf-old.ini:/usr/local/etc/php/conf.d/99optional.ini \
    chocin/php:5.6-apache-xdebug