#!/bin/bash
docker run -d \
    --name mysql56 \
    -p 3356:3306 \
    -v ../data/mysql/56:/var/lib/mysql \
    -e MYSQL_ROOT_PASSWORD=root \
    -e TZ=Asia/Jakarta \
    mysql:5.6 \
    --sql_mode= \
    --max_allowed_packet=32505856 \
    --default-time-zone=+07:00