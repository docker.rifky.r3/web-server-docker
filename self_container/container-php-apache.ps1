$currentDir = ${PWD};
$rootDir = Resolve-Path (Join-Path $currentDir '..');

docker run -d --add-host host.docker.internal:host-gateway `
    --name php82-apache `
    -p 8082:80 `
    -v $rootDir/data:/var/www/html `
    -v $rootDir/config/conf-new.ini:/usr/local/etc/php/conf.d/99optional.ini `
    chocin/php:8.2-apache-xdebug