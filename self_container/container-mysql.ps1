$currentDir = ${PWD};
$rootDir = Resolve-Path (Join-Path $currentDir '..');

docker run -d `
    --name mysql80 `
    -p 3380:3306 `
    -v $rootDir/data/mysql/80:/var/lib/mysql `
    -e MYSQL_ROOT_PASSWORD=root `
    -e TZ=Asia/Jakarta `
    mysql:8.0-debian `
    --sql_mode= `
    --max_allowed_packet=32505856 `
    --default-time-zone=+07:00 `
    --authentication-policy=mysql_native_password