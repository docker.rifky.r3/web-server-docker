#!/bin/bash

#If .env file does not exist, copy it
if test -f .env
then 
    cp .env-sample .env
fi

docker-compose --env-file .env up -d nginx php-8.2-fpm mysql-8.0