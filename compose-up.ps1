#If .env file does not exist, copy it
if (-not(Test-Path -Path ${PWD}/.env -PathType Leaf)) {
    Copy-Item .env.sample .env
}
docker-compose --env-file .env up -d nginx php-8.2-fpm mysql-8.0